#!/usr/bin/make -f

# BSD grep does not support all the options
#version != grep -oPm1 "(?<=<version>)[^<]+" src/mod_slideshow.xml | tr -d "[:space:]"
version = $(shell sed -ne 's/.*<version>\(.*\)<\/version>.*/\1/p' src/mod_slideshow.xml)

release: release/mod_slideshow-${version}.zip

release/mod_slideshow-${version}.zip:
	mkdir -p release
	cd src && zip -r ../release/mod_slideshow-${version}.zip .
