# SlideShow

SlideShow is a simple but powerful Joomla! module for generating nice teasers.
You can find a real world example at https://apostel-harburg.de. This module
requires at least Joomla! 3.6 but has only been tested with v3.8.