function Slideshow(id, duration, autoSlide, controls) {
  this.element = document.getElementById(id);
  this.current = 0;
  this.slides = this.element.children[0].children;
  this.count = this.slides.length;
  this.controls = controls;

  this.duration = duration;
  this.autoSlide = autoSlide;

  if (this.controls) {
    var prev = document.createElement('i');
    prev.classList.add('icon-chevron-left');
    prev.addEventListener('click', function() {
      this.prev();
    }.bind(this));

    var next = document.createElement('i');
    next.classList.add('icon-chevron-right');
    next.addEventListener('click', function() {
      this.next();
    }.bind(this));

    this.element.appendChild(prev);
    this.element.appendChild(next);

    this.gotoSlideControl = document.createElement('div');
    this.gotoSlideControl.classList.add("ctrl-group");
    this.gotoSlideControl.style.marginLeft = (-this.count * 7) + 'px';
    for (var i = 0; i < this.count; i++) {
      var ctrl = document.createElement('span');
      ctrl.classList.add("ctrl");
      ctrl.i = i;
      if (this.current == i)
        ctrl.classList.add("current");

      ctrl.addEventListener('click', function(e) {
        this.gotoSlide(e.target.i);
      }.bind(this));

      this.gotoSlideControl.appendChild(ctrl);
    }
    this.element.appendChild(this.gotoSlideControl);
  }

  this.prev = function() {
    this.slides[this.current].classList.remove("show");
    if (this.controls)
      this.gotoSlideControl.children[this.current].classList.remove("current");

    this.current--;
    if (this.current < 0)
      this.current = this.count - 1;

    this.slides[this.current].classList.add("show");
    if (this.controls)
      this.gotoSlideControl.children[this.current].classList.add("current");

    if (this.autoSlide) {
      clearTimeout(this.timer);
      this.timer = setTimeout(this.next, this.duration);
    }
  };

  this.next = function() {
    this.slides[this.current].classList.remove("show");
    if (this.controls)
      this.gotoSlideControl.children[this.current].classList.remove("current");

    this.current++;
    if (this.current >= this.count)
      this.current = 0;

    this.slides[this.current].classList.add("show");
    if (this.controls)
      this.gotoSlideControl.children[this.current].classList.add("current");

    if (this.autoSlide) {
      clearTimeout(this.timer);
      this.timer = setTimeout(this.next, this.duration);
    }
  };

  this.gotoSlide = function(slide) {
    this.slides[this.current].classList.remove("show");
    this.gotoSlideControl.children[this.current].classList.remove("current");

    this.current = slide;

    this.slides[this.current].classList.add("show");
    this.gotoSlideControl.children[this.current].classList.add("current");

    if (this.autoSlide) {
      clearTimeout(this.timer);
      this.timer = setTimeout(this.next, this.duration);
    }
  };

  if (this.autoSlide)
    this.timer = setTimeout(this.next, this.duration);
}
