<?php

defined('_JEXEC') or die;

$slides = $params->get('slides');
$id = $params->get('id', 'slideshow-1');
$modHeight = $params->get('modh', '100vh');
$autoSlide = $params->get('auto-slide', 'true');
$duration = $params->get('duration', '10000');
$controls = $params->get('controls', 'true');
$html = "";
$first = true;

foreach ($slides as &$s) {
  if ($s->enable == "0")
    continue;
  if ($s->uselink == "1")
    $html .= '<a href="' . JRoute::_("index.php?Itemid=" . $s->link) . '"';
  else
    $html .= '<a';
  if ($first) {
    $html .= ' class="show"';
    $first = false;
  }
  $html .= ">
<div class=\"slideshow-img\" style=\"background-image:url($s->img);"
         . "background-position:$s->imgax $s->imgay\"></div>
<div style=\"width:$s->t0w;max-width:$s->t0max;top:$s->t0pt;left:$s->t0pl;"
         . "right:$s->t0pr;bottom:$s->t0pb;color:$s->t0c;"
         . "background-color:$s->t0b;padding:$s->t0p;text-shadow:$s->t0ts;"
         . "box-shadow:$s->t0bs\">$s->t0</div>";
  if (!ctype_space($s->t1)) {
    $html .= "
<div style=\"width:$s->t1w;max-width:$s->t1max;top:$s->t1pt;left:$s->t1pl;"
           . "right:$s->t1pr;bottom:$s->t1pb;color:$s->t1c;"
           . "background-color:$s->t1b;padding:$s->t1p;text-shadow:$s->t1ts;"
           . "box-shadow:$s->t1bs\">$s->t1</div>";
  }
  $html .= "
</a>";
}

require JModuleHelper::getLayoutPath('mod_slideshow');

?>
