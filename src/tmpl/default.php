<?php

defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::base()
                             . 'modules/mod_slideshow/tmpl/default.css');
$document->addScript(JUri::base() . 'modules/mod_slideshow/slideshow.js');

$document->addScriptDeclaration("
window.addEventListener('load', function() {
  Slideshow('$id', $duration, $autoSlide, $controls);
});
");

?>
<div class="slideshow-container" id="<?php echo $id; ?>"
     style="height:<?php echo $modHeight; ?>">
  <div class="slideshow-slides" style="height:<?php echo $modHeight; ?>">
    <?php echo $html; ?>
  </div>
</div>